package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.util.BpelCache;
import org.apache.commons.jxpath.Function;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public abstract class AbstractCacheFunction implements Function {
    
    protected final BpelCache bpelCache;
    
    public AbstractCacheFunction(BpelCache bpelCache) {
        this.bpelCache = bpelCache;
    }
}
